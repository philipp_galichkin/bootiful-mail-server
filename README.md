#### A Spring Boot &  SendGrind based mail server

##### Implementation notes
* Language: **Java**
* Framework: **Spring Boot**
* Mail: **SendGrid**

The application consists of two main parts: a mail sending service and a guaranteed delivery monitoring.
I used AOP to separate mail sending logic from the delivery monitoring. 
Thus, in case the guaranteed delivery is no longer a requirement, we can easily delete the `delivery` package and that's it.
Top level packages can also become Maven modules without hassle.

In general, I utilized a common layered architecture with `domain`, `web`, `service` packages. 
There are/could be packages that support infrastructure: configs, converters, aspects, and so on.

There are REST APIs for sending emails and handling callbacks (webhooks) from the SendGrid.

For the sake of time saving I decided to not to go all-in in guaranteed delivery implementation.
The main idea is to persist mail requests with states and update those states according to responses and event callbacks
from SendGrid. I think I'd use Mongo to store not yet delivered emails with states. 
After a successful delivery the data can be cleaned up to save disk space.

###### Future improvements

* Finish off the guaranteed delivery.
* Naming, naming, naming... there is always a room for improvements. 
* Packages shuffling to make architecture more screaming
* More testing
* Dokerize it
* CI/CD
* Extract credentials and keys in maven's `settings.xml` for more security.
 

##### Build & Run
###### Maven
1. Make sure you have maven installed
2. `cd /<path-to-project>/deloitte`
3. `mvn clean compile verify spring-boot:run`

The app will execute all tests and start on port `8080`

###### Intellij IDEA
1. Make sure you have the best IDE in the World installed
2. Go to Run/Debug Configuration 
3. Add new Spring Boot configuration and select a main class
`ru.galichkin.deloitte.Application`
4. Run/Debug the app

The app will start on port `8080`

##### Manual testing

1. Follow https://documenter.getpostman.com/view/4505152/S1EJYMbd
2. Click 'Run in Postman' or copy and execute `curl` scripts
3. The `/mail` resource is protected with a basic auth, so don't forget an `Authorization` header.
Credentials are configured as properties `service.username` and `service.password` in `application.yml`.

##### Other

There is a Bitbucket pipeline that executes tests on every push
https://bitbucket.org/philipp_galichkin/deloitte/addon/pipelines/home

Post completion bonus: SonarCloud report (I have never used Sonar's cloud service before, so I thought this project would an ideal guinea-pig to give it a try)
https://sonarcloud.io/dashboard?id=philipp_galichkin_deloitte