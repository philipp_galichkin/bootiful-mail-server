package ru.galichkin.deloitte.common;

import org.junit.Test;

import static org.assertj.core.api.Assertions.assertThat;

public class UrlResourcesTest {

    @Test
    public void shouldReturnCorrectUrls(){
        assertThat(UrlResources.EMAIL_RESOURCE_URL).isEqualTo("/mail");
        assertThat(UrlResources.EMAIL_CALLBACK_RESOURCE_URL).isEqualTo("/mail/callback");
    }
}