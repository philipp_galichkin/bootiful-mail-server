package ru.galichkin.deloitte.mailserver.web;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.BeforeClass;
import org.junit.Test;
import ru.galichkin.deloitte.common.config.JsonConfiguration;

import java.io.IOException;

import static org.assertj.core.api.Assertions.assertThat;

public class WhenConvertingJsonToMailRequestTest {

    private static ObjectMapper objectMapper;

    @BeforeClass
    public static void setUp() {
        objectMapper = new JsonConfiguration().objectMapper();
    }

    @Test
    public void shouldBeConvertedToCorrectMailRequest() throws IOException {

        //language=JSON
        String json = "{\"from\":\"sender@test.test\",\"tos\":[\"recipient@test.test\"],\"ccs\":[\"ccs1@test.test\",\"ccs2@test.test\"],\"bccs\":[\"bccs@test.test\"],\"subject\":\"subject\",\"body\":\"body\"}";

        final MailRequest mailRequest = objectMapper.readValue(json, MailRequest.class);

        assertThat(mailRequest.getFrom()).isEqualTo("sender@test.test");
        assertThat(mailRequest.getTos()).containsExactly("recipient@test.test");
        assertThat(mailRequest.getSubject()).isEqualTo("subject");
        assertThat(mailRequest.getBody()).isEqualTo("body");
        assertThat(mailRequest.getCcs()).containsExactly("ccs1@test.test", "ccs2@test.test");
        assertThat(mailRequest.getBccs()).containsExactly("bccs@test.test");
    }

}