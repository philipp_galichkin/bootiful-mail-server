package ru.galichkin.deloitte.mailserver.web;

import org.junit.Before;
import org.junit.Test;
import ru.galichkin.deloitte.mailserver.domain.EmailAddress;
import ru.galichkin.deloitte.mailserver.domain.EmailAddresses;

import javax.validation.ConstraintViolation;
import javax.validation.Validation;
import javax.validation.Validator;
import javax.validation.ValidatorFactory;
import java.util.Set;

import static org.assertj.core.api.Assertions.assertThat;
import static ru.galichkin.deloitte.mailserver.domain.RequestValidationConstants.*;

public class WhenValidatingMailRequestTest {

    private static final EmailAddress INVALID_EMAIL = new EmailAddress("invalid_email");
    private static final String MUST_NOT_BE_NULL_VALIDATION_MESSAGE = "must not be null";


    private Validator validator;
    private MailRequest request = new TestMailRequest();

    @Before
    public void setUp() {
        ValidatorFactory factory = Validation.buildDefaultValidatorFactory();
        validator = factory.getValidator();
    }

    @Test
    public void shouldValidateFromWithNull() {
        request.setFrom(null);
        Set<ConstraintViolation<MailRequest>> violations = validator.validate(request);
        assertThat(violations.isEmpty()).isFalse();
        assertThat(violations.iterator().next().getMessage()).isEqualTo(SENDER_VALIDATION_MESSAGE);
    }

    @Test
    public void shouldValidateFromWithInvalidEmailAddress() {
        request.setFrom(INVALID_EMAIL);
        Set<ConstraintViolation<MailRequest>> violations = validator.validate(request);
        assertThat(violations.isEmpty()).isFalse();
        assertThat(violations.iterator().next().getMessage()).isEqualTo(EMAIL_ADDRESS_VALIDATION_MESSAGE);
    }

    @Test
    public void shouldValidateEmailAddressesWithNull() {
        request.setTos(null);
        Set<ConstraintViolation<MailRequest>> violations = validator.validate(request);
        assertThat(violations.isEmpty()).isFalse();
        assertThat(violations.iterator().next().getMessage()).isEqualTo(MUST_NOT_BE_NULL_VALIDATION_MESSAGE);
    }

    @Test
    public void shouldValidateEmailAddressesWithInvalidEmailAddress() {
        request.setTos(new EmailAddresses().add(INVALID_EMAIL));
        Set<ConstraintViolation<MailRequest>> violations = validator.validate(request);
        assertThat(violations.isEmpty()).isFalse();
        assertThat(violations.iterator().next().getMessage()).isEqualTo(EMAIL_ADDRESS_VALIDATION_MESSAGE);
    }

    @Test
    public void shouldValidateSubjectWithBlankString() {
        request.setSubject("");
        Set<ConstraintViolation<MailRequest>> violations = validator.validate(request);
        assertThat(violations.isEmpty()).isFalse();
        assertThat(violations.iterator().next().getMessage()).isEqualTo(SUBJECT_VALIDATION_MESSAGE);
    }

    @Test
    public void shouldValidateSubjectWithNull() {
        request.setSubject(null);
        Set<ConstraintViolation<MailRequest>> violations = validator.validate(request);
        assertThat(violations.isEmpty()).isFalse();
        assertThat(violations.iterator().next().getMessage()).isEqualTo(SUBJECT_VALIDATION_MESSAGE);
    }

    @Test
    public void shouldValidateBodyWithBlankString() {
        request.setBody("");
        Set<ConstraintViolation<MailRequest>> violations = validator.validate(request);
        assertThat(violations.isEmpty()).isFalse();
        assertThat(violations.iterator().next().getMessage()).isEqualTo(BODY_VALIDATION_MESSAGE);
    }

    @Test
    public void shouldValidateBodyWithNull() {
        request.setBody(null);
        Set<ConstraintViolation<MailRequest>> violations = validator.validate(request);
        assertThat(violations.isEmpty()).isFalse();
        assertThat(violations.iterator().next().getMessage()).isEqualTo(BODY_VALIDATION_MESSAGE);
    }
}