package ru.galichkin.deloitte.mailserver.web;

import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.extern.slf4j.Slf4j;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.context.annotation.Import;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import ru.galichkin.deloitte.common.UrlResources;
import ru.galichkin.deloitte.mailserver.config.TestConfig;
import ru.galichkin.deloitte.mailserver.util.Json;

import static org.assertj.core.api.Assertions.assertThat;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;

@Slf4j
@ActiveProfiles("test")
@RunWith(SpringRunner.class)
@Import(TestConfig.class)
@WebMvcTest(MailController.class)
public class WhenCallingEmailResourceITTest {

    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private ObjectMapper objectMapper;

    @Test
    public void shouldReturnOkResponse() throws Exception {
        String content = new Json(objectMapper).asJsonString(new TestMailRequest());
        MvcResult result = execute(content);
        assertThat(result.getResponse().getStatus()).isEqualTo(HttpStatus.OK.value());
    }

    @Test
    public void shouldReturnBadRequestOnInvalidRequest() throws Exception {
        TestMailRequest testMail = new TestMailRequest();
        testMail.setFrom(null);
        String content = new Json(objectMapper).asJsonString(testMail);
        MvcResult result = execute(content);
        assertThat(result.getResponse().getStatus()).isEqualTo(HttpStatus.BAD_REQUEST.value());
    }

    private MvcResult execute(String content) throws Exception {
        return mockMvc.perform(
                MockMvcRequestBuilders.post(
                        UrlResources.EMAIL_RESOURCE_URL)
                        .contentType(MediaType.APPLICATION_JSON_UTF8)
                        .accept(MediaType.APPLICATION_JSON_UTF8)
                        .content(content))
                .andDo(print())
                .andReturn();
    }
}