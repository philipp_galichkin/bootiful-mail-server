package ru.galichkin.deloitte.mailserver.web;

import ru.galichkin.deloitte.mailserver.domain.EmailAddress;
import ru.galichkin.deloitte.mailserver.domain.EmailAddresses;

public class TestMailRequest extends MailRequest {
    public TestMailRequest() {
        setSubject("subject");
        setBody("body");
        setFrom(new EmailAddress("sender@test.test"));
        setTos(new EmailAddresses().add(new EmailAddress("recipient@test.test")));
        setCcs(new EmailAddresses().add(new EmailAddress("ccs@test.test")));
        setBccs(new EmailAddresses().add(new EmailAddress("bccs@test.test")));
    }
}
