package ru.galichkin.deloitte.mailserver.web;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import ru.galichkin.deloitte.mailserver.service.EmailService;

import static org.mockito.Mockito.verify;

@RunWith(MockitoJUnitRunner.class)
public class WhenCallingEmailControllerSendMethodTest {

    @Mock
    EmailService service;
    @InjectMocks
    MailController controller;

    @Test
    public void shouldCallService() {
        final TestMailRequest email = new TestMailRequest();
        controller.send(email);
        verify(service).send(email);
    }
}