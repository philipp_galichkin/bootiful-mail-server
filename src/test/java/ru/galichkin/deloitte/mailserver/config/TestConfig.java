package ru.galichkin.deloitte.mailserver.config;

import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.sendgrid.SendGridProperties;
import org.springframework.context.annotation.*;
import ru.galichkin.deloitte.common.config.EmailServiceProperties;
import ru.galichkin.deloitte.common.config.JsonConfiguration;
import ru.galichkin.deloitte.common.config.SecurityConfiguration;
import ru.galichkin.deloitte.mailserver.service.EmailService;
import ru.galichkin.deloitte.mailserver.service.FakeEmailService;

@Configuration
@PropertySource("classpath:application.yml")
@EnableAutoConfiguration
@Import({EmailServiceProperties.class, JsonConfiguration.class, SecurityConfiguration.class, SendGridProperties.class})
public class TestConfig {

    @Profile("test")
    @Bean
    public EmailService fakeEmailService(){
        return new FakeEmailService();
    }
}
