package ru.galichkin.deloitte.mailserver.service;

import lombok.extern.slf4j.Slf4j;
import ru.galichkin.deloitte.common.domain.BasicMail;
import ru.galichkin.deloitte.mailserver.domain.SendEmailResponse;

@Slf4j
public class FakeEmailService implements EmailService {
    @Override
    public SendEmailResponse send(BasicMail email) {
        log.info("Fake email has been sent: {}", email);
        return new SendEmailResponse();
    }
}
