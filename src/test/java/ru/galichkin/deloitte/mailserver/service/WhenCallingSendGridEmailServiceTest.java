package ru.galichkin.deloitte.mailserver.service;

import lombok.extern.slf4j.Slf4j;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.SpyBean;
import org.springframework.context.annotation.Import;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import ru.galichkin.deloitte.mailserver.config.SendGridProperties;
import ru.galichkin.deloitte.mailserver.config.TestConfig;
import ru.galichkin.deloitte.mailserver.domain.SendEmailResponse;
import ru.galichkin.deloitte.mailserver.exception.ErrorCode;
import ru.galichkin.deloitte.mailserver.exception.ErrorResponse;
import ru.galichkin.deloitte.mailserver.web.TestMailRequest;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.when;

@Slf4j
@ActiveProfiles("it")
@Import(TestConfig.class)
@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.NONE)
public class WhenCallingSendGridEmailServiceTest {

    @SpyBean
    private SendGridProperties properties;

    private SendGridEmailService service;

    @Before
    public void setUp() {
        service = new SendGridEmailService(properties);
    }

    @Test
    public void shouldSentRealEmailSuccessfully() {
        final TestMailRequest email = new TestMailRequest();
        final SendEmailResponse response = service.send(email);

        assertThat(response.isSuccessful()).isTrue();
    }

    @Test
    public void shouldReturn403WithInvalidApiKey() {
        when(properties.getKey()).thenReturn("invalid_key");

        final TestMailRequest email = new TestMailRequest();
        final SendEmailResponse response = service.send(email);

        assertThat(response.isSuccessful()).isFalse();
        assertThat(response.getStatusCode()).isEqualTo(401);
        final ErrorResponse error = response.getError();
        log.info("Error: {}", error);
        assertThat(error).isNotNull();
        assertThat(error.getCode()).isEqualTo(ErrorCode.SENDGRID_ERR);
    }
}