package ru.galichkin.deloitte.mailserver.service;

import com.sendgrid.Mail;
import com.sendgrid.Method;
import org.junit.Test;
import ru.galichkin.deloitte.mailserver.domain.SendGridMail;
import ru.galichkin.deloitte.mailserver.web.TestMailRequest;

import static org.assertj.core.api.Assertions.assertThat;


public class WhenCreatingSendGridMailPostRequestTest {

    @Test
    public void shouldCreateCorrectPostRequest() {

        final TestMailRequest testMailRequestBody = new TestMailRequest();
        final Mail mail = new SendGridMail(testMailRequestBody);

        SendGridMailPostRequest request = new SendGridMailPostRequest(mail);

        assertThat(request.getEndpoint()).isEqualTo("mail/send");
        assertThat(request.getMethod()).isEqualTo(Method.POST);
        assertThat(request.getBody()).contains(testMailRequestBody.getBody());
    }
}