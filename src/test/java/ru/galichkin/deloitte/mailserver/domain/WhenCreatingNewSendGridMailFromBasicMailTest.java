package ru.galichkin.deloitte.mailserver.domain;

import com.sendgrid.Content;
import com.sendgrid.Email;
import com.sendgrid.Personalization;
import org.assertj.core.util.Lists;
import org.junit.Before;
import org.junit.Test;
import ru.galichkin.deloitte.common.domain.BasicMail;

import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

import static org.assertj.core.api.Assertions.assertThat;

public class WhenCreatingNewSendGridMailFromBasicMailTest {

    private SendGridMail sendGridMail;
    private TestBasicMail testBasicMail;

    @Before
    public void setUp() {
        testBasicMail = new TestBasicMail();
        sendGridMail = new SendGridMail(testBasicMail);
    }

    @Test
    public void shouldSetCorrectSubject() {
        assertThat(sendGridMail.getSubject()).isEqualTo(testBasicMail.getSubject());
    }

    @Test
    public void shouldSetCorrectSender() {
        assertThat(sendGridMail.getFrom().getEmail()).isEqualTo(testBasicMail.getFrom());
    }

    @Test
    public void shouldSetCorrectRecipients() {
        final List<Personalization> personalization = sendGridMail.getPersonalization();
        assertThat(personalization.size()).isEqualTo(1);
        assertThat(emailsToStrings(personalization.get(0).getTos())).containsExactlyInAnyOrder(stringsToArray(testBasicMail.getTos()));
    }

    @Test
    public void shouldSetCorrectCcs() {
        final List<Personalization> personalization = sendGridMail.getPersonalization();
        assertThat(personalization.size()).isEqualTo(1);
        assertThat(emailsToStrings(personalization.get(0).getCcs())).containsExactlyInAnyOrder(stringsToArray(testBasicMail.getCcs()));
    }

    @Test
    public void shouldSetCorrectBcs() {
        final List<Personalization> personalization = sendGridMail.getPersonalization();
        assertThat(personalization.size()).isEqualTo(1);
        assertThat(emailsToStrings(personalization.get(0).getBccs())).containsExactlyInAnyOrder(stringsToArray(testBasicMail.getBccs()));
    }

    @Test
    public void shouldSetCorrectContent() {
        final List<Content> content = sendGridMail.getContent();
        assertThat(content.size()).isEqualTo(1);
        assertThat(content.get(0).getValue()).isEqualTo(testBasicMail.getBody());
        assertThat(content.get(0).getType()).isEqualTo(SendGridMail.TEXT_BODY_CONTENT_TYPE);

    }

    private String[] stringsToArray(List<String> listOfString) {
        return listOfString.toArray(new String[]{});
    }

    private List<String> emailsToStrings(List<Email> listOfEmails) {
        return listOfEmails.stream().map(Email::getEmail).collect(Collectors.toList());
    }

    private static class TestBasicMail implements BasicMail {
        @Override
        public UUID getId() {
            return UUID.randomUUID();
        }

        @Override
        public String getFrom() {
            return "from";
        }

        @Override
        public List<String> getTos() {
            return Lists.newArrayList("to1", "to2");
        }

        @Override
        public List<String> getCcs() {
            return Lists.newArrayList("cc1", "cc2");
        }

        @Override
        public List<String> getBccs() {
            return Lists.newArrayList("bc1", "bc2");
        }

        @Override
        public String getSubject() {
            return "subject";
        }

        @Override
        public String getBody() {
            return "body";
        }
    }
}