package ru.galichkin.deloitte.delivery.aspect;

import org.aspectj.lang.ProceedingJoinPoint;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import ru.galichkin.deloitte.delivery.service.EmailGuaranteedDeliveryService;
import ru.galichkin.deloitte.mailserver.domain.SendEmailResponse;
import ru.galichkin.deloitte.mailserver.web.TestMailRequest;

import static org.mockito.Mockito.*;

@RunWith(MockitoJUnitRunner.class)
public class DeliveryAspectTest {

    @Mock
    private EmailGuaranteedDeliveryService service;

    @Mock
    private ProceedingJoinPoint proceedingJoinPoint;

    private DeliveryAspect sampleAspect;

    private TestMailRequest email = new TestMailRequest();

    @Before
    public void setUp() {
        sampleAspect = new DeliveryAspect(service);
    }

    @Test
    public void shouldLogSuccessfulResponseCorrectly() throws Throwable {
        SendEmailResponse mockResponse = mock(SendEmailResponse.class);
        when(mockResponse.isSuccessful()).thenReturn(true);
        when(mockResponse.getMessageId()).thenReturn("mes_id");
        when(proceedingJoinPoint.proceed()).thenReturn(mockResponse);

        sampleAspect.around(proceedingJoinPoint, email);

        verify(service).markCreated(email);
        verify(service).markAccepted(email, mockResponse.getMessageId());
        verify(service, never()).markError(email, mockResponse.getMessageId());
    }

    @Test
    public void shouldLogErrorResponseCorrectly() throws Throwable {
        SendEmailResponse mockResponse = mock(SendEmailResponse.class);
        when(mockResponse.isSuccessful()).thenReturn(false);

        when(proceedingJoinPoint.proceed()).thenReturn(mockResponse);

        sampleAspect.around(proceedingJoinPoint, email);

        verify(service).markCreated(email);
        verify(service).markError(email, null);
        verify(service, never()).markAccepted(email, mockResponse.getMessageId());
    }
}