package ru.galichkin.deloitte.delivery.service;

import org.assertj.core.util.Lists;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import ru.galichkin.deloitte.delivery.web.SendGridCallback;

import java.util.List;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

@RunWith(MockitoJUnitRunner.class)
public class WhenProcessingCallbacksTest {

    @Mock
    private EmailGuaranteedDeliveryService deliveryService;

    @InjectMocks
    private CallbackProcessingServiceImpl processingService;

    @Test
    public void shouldProcessAllCallbackUsingDeliveryService() {
        List<SendGridCallback> callbacks = Lists.newArrayList(
                new SendGridCallback(),
                new SendGridCallback(),
                new SendGridCallback()
        );

        processingService.process(callbacks);

        verify(deliveryService, times(callbacks.size())).mark(any(), any(), any());
    }
}