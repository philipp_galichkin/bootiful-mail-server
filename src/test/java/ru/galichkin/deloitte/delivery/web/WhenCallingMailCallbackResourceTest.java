package ru.galichkin.deloitte.delivery.web;

import lombok.extern.slf4j.Slf4j;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.context.annotation.Import;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import ru.galichkin.deloitte.common.UrlResources;
import ru.galichkin.deloitte.delivery.config.DeliveryConfig;

import static org.assertj.core.api.Assertions.assertThat;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;

@Slf4j
@ActiveProfiles("test")
@RunWith(SpringRunner.class)
@Import(DeliveryConfig.class)
@WebMvcTest(SendGridMailCallbackController.class)
public class WhenCallingMailCallbackResourceTest {

    @Autowired
    private MockMvc mockMvc;

    @Test
    public void shouldAcceptCallbacks() throws Exception {
        String callbackJson = "[{\"email\":\"ccs@test.test\",\"event\":\"dropped\",\"reason\":\"Invalid\",\"sg_event_id\":\"ZHJvcC0xMDA2MzA5MS00VTF3amlCNVRiQ09tN0lTYmlNclF3LTE\",\"sg_message_id\":\"4U1wjiB5TbCOm7ISbiMrQw.filter0096p3iad2-3674-5CAA0009-8.1\",\"smtp-id\":\"<4U1wjiB5TbCOm7ISbiMrQw@ismtpd0002p1lon1.sendgrid.net>\",\"timestamp\":1554645002},\n" +
                "{\"email\":\"bccs@test.test\",\"event\":\"dropped\",\"reason\":\"Invalid\",\"sg_event_id\":\"ZHJvcC0xMDA2MzA5MS00VTF3amlCNVRiQ09tN0lTYmlNclF3LTI\",\"sg_message_id\":\"4U1wjiB5TbCOm7ISbiMrQw.filter0096p3iad2-3674-5CAA0009-8.2\",\"smtp-id\":\"<4U1wjiB5TbCOm7ISbiMrQw@ismtpd0002p1lon1.sendgrid.net>\",\"timestamp\":1554645002},\n" +
                "{\"email\":\"recipient@test.test\",\"event\":\"dropped\",\"reason\":\"Invalid\",\"sg_event_id\":\"ZHJvcC0xMDA2MzA5MS00VTF3amlCNVRiQ09tN0lTYmlNclF3LTA\",\"sg_message_id\":\"4U1wjiB5TbCOm7ISbiMrQw.filter0096p3iad2-3674-5CAA0009-8.0\",\"smtp-id\":\"<4U1wjiB5TbCOm7ISbiMrQw@ismtpd0002p1lon1.sendgrid.net>\",\"timestamp\":1554645002}]";
        MvcResult result = execute(callbackJson);
        assertThat(result.getResponse().getStatus()).isEqualTo(HttpStatus.OK.value());
    }

    private MvcResult execute(String content) throws Exception {
        return mockMvc.perform(
                MockMvcRequestBuilders.post(
                        UrlResources.EMAIL_CALLBACK_RESOURCE_URL)
                        .contentType(MediaType.APPLICATION_JSON_UTF8)
                        .accept(MediaType.APPLICATION_JSON_UTF8)
                        .content(content))
                .andDo(print())
                .andReturn();
    }
}