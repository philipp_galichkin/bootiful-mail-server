package ru.galichkin.deloitte.delivery.config;

import lombok.extern.slf4j.Slf4j;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;

@Profile("test")
@Slf4j
@ComponentScan({"ru.galichkin.deloitte.delivery", "ru.galichkin.deloitte.common"})
@Configuration
public class DeliveryConfig {
}


