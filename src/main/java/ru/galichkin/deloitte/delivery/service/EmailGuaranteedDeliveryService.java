package ru.galichkin.deloitte.delivery.service;

import ru.galichkin.deloitte.common.domain.BasicMail;

/**
 * This service communicates with some persistance layer to save and retrieve emails with their states.
 * <br/>
 * It also has a scheduler that re-delivers messages.
 */
public interface EmailGuaranteedDeliveryService {

    void markCreated(BasicMail mail);

    void markAccepted(BasicMail mail, String messageId);

    void markError(BasicMail mail, String messageId);

    //This is for an architecture mockup only
    void mark(String email, String sgMessageId, String event);

    //Mark @Scheduled(...)
    void redeliverFailed();
}
