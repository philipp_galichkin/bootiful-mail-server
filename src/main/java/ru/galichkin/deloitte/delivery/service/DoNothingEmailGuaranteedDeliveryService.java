package ru.galichkin.deloitte.delivery.service;

import lombok.extern.slf4j.Slf4j;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;
import ru.galichkin.deloitte.common.domain.BasicMail;

@Slf4j
@Service
public class DoNothingEmailGuaranteedDeliveryService implements EmailGuaranteedDeliveryService {

    @Override
    public void markCreated(BasicMail mail) {
        log.info("markCreated: {}", mail);
    }

    @Override
    public void markAccepted(BasicMail mail, String messageId) {
        log.info("markAccepted: {}, {}", mail, messageId);
    }

    @Override
    public void markError(BasicMail mail, String messageId) {
        log.info("markError: {}, {}", mail, messageId);
    }

    @Override
    public void mark(String email, String sgMessageId, String event) {
        log.info("mark: {}, {}, {}", email, sgMessageId, event);
    }

    @Scheduled(cron = "0 0 0 0 0 *")
    @Override
    public void redeliverFailed() {
    }
}
