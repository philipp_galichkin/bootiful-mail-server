package ru.galichkin.deloitte.delivery.service;

import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import ru.galichkin.deloitte.delivery.web.SendGridCallback;

import java.util.List;

@Slf4j
@Service
@AllArgsConstructor
public class CallbackProcessingServiceImpl implements CallbackProcessingService {

    private final EmailGuaranteedDeliveryService deliveryService;

    @Override
    public void process(List<SendGridCallback> callback) {
        callback.forEach(this::process);
    }

    private void process(SendGridCallback c) {
        //extract ids and states from callbacks
        //update via deliveryService
        deliveryService.mark(c.getEmail(), c.getSgMessageId(), c.getEvent());
    }
}
