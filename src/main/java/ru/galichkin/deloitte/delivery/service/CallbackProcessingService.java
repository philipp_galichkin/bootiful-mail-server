package ru.galichkin.deloitte.delivery.service;

import ru.galichkin.deloitte.delivery.web.SendGridCallback;

import java.util.List;

public interface CallbackProcessingService {
    void process(List<SendGridCallback> callback);
}
