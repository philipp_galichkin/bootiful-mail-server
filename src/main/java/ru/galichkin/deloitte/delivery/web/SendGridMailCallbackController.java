package ru.galichkin.deloitte.delivery.web;

import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import ru.galichkin.deloitte.common.UrlResources;
import ru.galichkin.deloitte.delivery.service.CallbackProcessingService;

import java.util.List;

import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;

@RestController
@RequestMapping(consumes = APPLICATION_JSON_VALUE, produces = APPLICATION_JSON_VALUE)
@RequiredArgsConstructor
public class SendGridMailCallbackController {

    private final CallbackProcessingService callbackProcessingService;

    @PostMapping(UrlResources.EMAIL_CALLBACK_RESOURCE_URL)
    void processCallback(@RequestBody List<SendGridCallback> callback) {
        callbackProcessingService.process(callback);
    }
}
