package ru.galichkin.deloitte.delivery.web;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

/**
 * [
 * {
 * "email": "ccs@test.test",
 * "event": "dropped",
 * "reason": "Invalid",
 * "sg_event_id": "ZHJvcC0xMDA2MzA5MS00VTF3amlCNVRiQ09tN0lTYmlNclF3LTE",
 * "sg_message_id": "4U1wjiB5TbCOm7ISbiMrQw.filter0096p3iad2-3674-5CAA0009-8.1",
 * "smtp-id": "<4U1wjiB5TbCOm7ISbiMrQw@ismtpd0002p1lon1.sendgrid.net>",
 * "timestamp": 1554645002
 * },
 * {
 * "email": "bccs@test.test",
 * "event": "dropped",
 * "reason": "Invalid",
 * "sg_event_id": "ZHJvcC0xMDA2MzA5MS00VTF3amlCNVRiQ09tN0lTYmlNclF3LTI",
 * "sg_message_id": "4U1wjiB5TbCOm7ISbiMrQw.filter0096p3iad2-3674-5CAA0009-8.2",
 * "smtp-id": "<4U1wjiB5TbCOm7ISbiMrQw@ismtpd0002p1lon1.sendgrid.net>",
 * "timestamp": 1554645002
 * },
 * {
 * "email": "recipient@test.test",
 * "event": "dropped",
 * "reason": "Invalid",
 * "sg_event_id": "ZHJvcC0xMDA2MzA5MS00VTF3amlCNVRiQ09tN0lTYmlNclF3LTA",
 * "sg_message_id": "4U1wjiB5TbCOm7ISbiMrQw.filter0096p3iad2-3674-5CAA0009-8.0",
 * "smtp-id": "<4U1wjiB5TbCOm7ISbiMrQw@ismtpd0002p1lon1.sendgrid.net>",
 * "timestamp": 1554645002
 * }
 * ]
 */
@Data
public class SendGridCallback {
    private String email;
    private String event;
    private String reason;
    @JsonProperty("sg_message_id")
    private String sgMessageId;
    private long timestamp;
}
