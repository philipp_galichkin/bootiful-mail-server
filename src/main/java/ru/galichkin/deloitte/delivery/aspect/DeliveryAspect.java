package ru.galichkin.deloitte.delivery.aspect;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.springframework.context.annotation.Configuration;
import ru.galichkin.deloitte.common.domain.BasicMail;
import ru.galichkin.deloitte.delivery.service.EmailGuaranteedDeliveryService;
import ru.galichkin.deloitte.mailserver.domain.SendEmailResponse;

@Slf4j
@Aspect
@Configuration
@RequiredArgsConstructor
public class DeliveryAspect {

    private final EmailGuaranteedDeliveryService emailGuaranteedDeliveryService;

    @Around(value = "execution(* ru.galichkin.deloitte.mailserver.service.EmailService.send(..)) && args(mail)", argNames = "joinPoint, mail")
    public Object around(ProceedingJoinPoint joinPoint, BasicMail mail) throws Throwable {
        emailGuaranteedDeliveryService.markCreated(mail);
        SendEmailResponse response = (SendEmailResponse) joinPoint.proceed();
        if (response.isSuccessful()) {
            emailGuaranteedDeliveryService.markAccepted(mail, response.getMessageId());
        } else {
            emailGuaranteedDeliveryService.markError(mail, response.getMessageId());
        }
        return response;
    }
}
