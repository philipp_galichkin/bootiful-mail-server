package ru.galichkin.deloitte.mailserver.web;

import lombok.RequiredArgsConstructor;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import ru.galichkin.deloitte.common.UrlResources;
import ru.galichkin.deloitte.mailserver.domain.SendEmailResponse;
import ru.galichkin.deloitte.mailserver.service.EmailService;

import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;

@RestController
@RequestMapping(consumes = APPLICATION_JSON_VALUE, produces = APPLICATION_JSON_VALUE)
@RequiredArgsConstructor
class MailController {

    private final EmailService service;

    @PostMapping(value = UrlResources.EMAIL_RESOURCE_URL)
    @ResponseBody
    SendEmailResponse send(@RequestBody @Validated MailRequest request) {
        return service.send(request);
    }
}
