package ru.galichkin.deloitte.mailserver.web;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import ru.galichkin.deloitte.common.domain.BasicMail;
import ru.galichkin.deloitte.mailserver.domain.EmailAddress;
import ru.galichkin.deloitte.mailserver.domain.EmailAddresses;

import javax.validation.Valid;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.util.List;
import java.util.UUID;

import static ru.galichkin.deloitte.mailserver.domain.RequestValidationConstants.*;

@ToString
@Setter
@NoArgsConstructor
class MailRequest implements BasicMail {
    @Valid
    @NotNull(message = SENDER_VALIDATION_MESSAGE)
    private EmailAddress from;
    @Valid
    @NotNull
    private EmailAddresses tos;
    @Valid
    @NotNull
    private EmailAddresses ccs;
    @Valid
    @NotNull
    private EmailAddresses bccs;
    @NotBlank(message = SUBJECT_VALIDATION_MESSAGE)
    private String subject;
    @NotBlank(message = BODY_VALIDATION_MESSAGE)
    private String body;

    @JsonIgnore
    @Override
    public UUID getId() {
        return UUID.randomUUID();
    }

    @Override
    public String getFrom() {
        return from.getEmail();
    }

    @Override
    public List<String> getTos() {
        return tos.getAsStrings();
    }

    @Override
    public List<String> getCcs() {
        return ccs.getAsStrings();
    }

    @Override
    public List<String> getBccs() {
        return bccs.getAsStrings();
    }

    @Override
    public String getSubject() {
        return subject;
    }

    @Override
    public String getBody() {
        return body;
    }


}
