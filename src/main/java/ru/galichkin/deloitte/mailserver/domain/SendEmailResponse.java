package ru.galichkin.deloitte.mailserver.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.sendgrid.Response;
import lombok.Data;
import ru.galichkin.deloitte.mailserver.exception.ErrorResponse;

import java.util.Map;

@Data
public class SendEmailResponse {

    public static final int SUCCESS_STATUS_CODE = 202;

    private ErrorResponse error;

    private int statusCode;

    private String body;

    private Map<String, String> headers;

    @JsonIgnore
    public static SendEmailResponse error(ErrorResponse error) {
        final SendEmailResponse response = new SendEmailResponse();
        response.setError(error);
        return response;
    }

    @JsonIgnore
    public static SendEmailResponse success(Response sendGridResponse) {
        final SendEmailResponse response = new SendEmailResponse();
        response.setBody(sendGridResponse.getBody());
        response.setHeaders(sendGridResponse.getHeaders());
        response.setStatusCode(sendGridResponse.getStatusCode());
        return response;
    }

    @JsonIgnore
    public static SendEmailResponse error(int statusCode, ErrorResponse errorResponse) {
        SendEmailResponse response = error(errorResponse);
        response.setStatusCode(statusCode);
        return response;
    }

    public boolean isSuccessful() {
        return statusCode == SUCCESS_STATUS_CODE && error == null;
    }

    @JsonIgnore
    public String getMessageId() {
        return headers.get("X-Message-Id");
    }
}
