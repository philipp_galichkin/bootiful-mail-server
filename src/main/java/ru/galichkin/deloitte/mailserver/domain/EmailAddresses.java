package ru.galichkin.deloitte.mailserver.domain;

import com.fasterxml.jackson.annotation.JsonCreator;
import lombok.ToString;
import org.springframework.validation.annotation.Validated;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@ToString
@Validated
public class EmailAddresses {

    @Valid
    private final List<EmailAddress> addresses;

    @JsonCreator
    public EmailAddresses(@NotNull List<EmailAddress> addresses) {
        this();
        this.addresses.addAll(addresses);
    }

    public EmailAddresses() {
        addresses = new ArrayList<>();
    }

    public List<String> getAsStrings() {
        return addresses.stream().map(EmailAddress::getEmail).collect(Collectors.toList());
    }

    public EmailAddresses add(EmailAddress address) {
        addresses.add(address);
        return this;
    }
}
