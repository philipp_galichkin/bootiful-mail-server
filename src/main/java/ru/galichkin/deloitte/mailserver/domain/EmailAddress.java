package ru.galichkin.deloitte.mailserver.domain;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonValue;
import lombok.ToString;

import javax.validation.Valid;
import javax.validation.constraints.Email;

import static ru.galichkin.deloitte.mailserver.domain.RequestValidationConstants.EMAIL_ADDRESS_VALIDATION_MESSAGE;
@ToString
@Valid
public class EmailAddress {

    @Email(message = EMAIL_ADDRESS_VALIDATION_MESSAGE)
    private String email;

    @JsonCreator
    public EmailAddress(@Valid @Email String email) {
        this.email = email;
    }

    @JsonValue
    public String getEmail() {
        return email;
    }
}
