package ru.galichkin.deloitte.mailserver.domain;

import lombok.AccessLevel;
import lombok.NoArgsConstructor;

@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class RequestValidationConstants {

    public static final String EMAIL_ADDRESS_VALIDATION_MESSAGE = "Must be valid email address";
    public static final String SUBJECT_VALIDATION_MESSAGE = "Subject must be set";
    public static final String SENDER_VALIDATION_MESSAGE = "Sender must be set";
    public static final String BODY_VALIDATION_MESSAGE = "Body must be set";
}
