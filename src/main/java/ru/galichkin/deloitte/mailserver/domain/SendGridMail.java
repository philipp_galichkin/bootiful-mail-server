package ru.galichkin.deloitte.mailserver.domain;

import com.sendgrid.Content;
import com.sendgrid.Email;
import com.sendgrid.Mail;
import com.sendgrid.Personalization;
import ru.galichkin.deloitte.common.domain.BasicMail;

public class SendGridMail extends Mail {

    static final String TEXT_BODY_CONTENT_TYPE = "text/plain";

    public SendGridMail(BasicMail email) {
        setFrom(new Email(email.getFrom()));
        setSubject(email.getSubject());
        addPersonalization(createPersonalization(email));
        addContent(new Content(TEXT_BODY_CONTENT_TYPE, email.getBody()));
    }

    private Personalization createPersonalization(BasicMail email) {
        final Personalization personalization = new Personalization();
        email.getTos().stream().map(Email::new).forEach(personalization::addTo);
        email.getCcs().stream().map(Email::new).forEach(personalization::addCc);
        email.getBccs().stream().map(Email::new).forEach(personalization::addBcc);
        return personalization;
    }
}