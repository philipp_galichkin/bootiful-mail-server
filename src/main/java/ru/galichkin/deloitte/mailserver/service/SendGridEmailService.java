package ru.galichkin.deloitte.mailserver.service;

import com.sendgrid.Response;
import com.sendgrid.SendGrid;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;
import ru.galichkin.deloitte.common.domain.BasicMail;
import ru.galichkin.deloitte.mailserver.config.SendGridProperties;
import ru.galichkin.deloitte.mailserver.domain.SendEmailResponse;
import ru.galichkin.deloitte.mailserver.domain.SendGridMail;
import ru.galichkin.deloitte.mailserver.exception.ErrorResponse;

import java.io.IOException;

import static ru.galichkin.deloitte.mailserver.exception.ErrorCode.SENDGRID_ERR;

@Slf4j
@Service
@RequiredArgsConstructor
class SendGridEmailService implements EmailService {

    private final SendGridProperties sendGridProperties;

    /**
     * Spring Retry can be used here to improve reliability
     *  when there are issues with connections or something like this.
     */
    @Override
    public SendEmailResponse send(BasicMail email) {
        SendGrid sg = new SendGrid(sendGridProperties.getKey());
        SendGridMail mail = new SendGridMail(email);
        SendGridMailPostRequest request = new SendGridMailPostRequest(mail);
        try {
            Response response = sg.api(request);
            logResponse(response);
            return SendEmailResponse.success(response);
        } catch (IOException ex) {
            return createErrorResponse(ex);
        }
    }

    private void logResponse(Response response) {
        log.info("{}", response.getStatusCode());
        log.info("{}", response.getBody());
        log.info("{}", response.getHeaders());
    }

    private SendEmailResponse createErrorResponse(IOException ex) {
        log.error("Error sending email", ex);
        final ErrorResponse errorResponse = new ErrorResponse(SENDGRID_ERR, ex.getMessage());
        int statusCode = extractStatusCodeFromUglyExceptionMessage(ex.getMessage());
        return SendEmailResponse.error(statusCode, errorResponse);
    }

    private int extractStatusCodeFromUglyExceptionMessage(String message) {
        //Request returned status Code 401Body:{"errors":[{"message":"The provided authorization grant is invalid, expired, or revoked","field":null,"help":null}]}
        final int indexOfStatusCodeThirdNumber = 29 + 3;
        if (StringUtils.isEmpty(message) || message.length() < indexOfStatusCodeThirdNumber) {
            return 0;
        }
        return Integer.valueOf(message.substring(indexOfStatusCodeThirdNumber - 3, indexOfStatusCodeThirdNumber));
    }
}
