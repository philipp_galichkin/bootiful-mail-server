package ru.galichkin.deloitte.mailserver.service;

import com.sendgrid.Mail;
import com.sendgrid.Method;
import com.sendgrid.Request;
import lombok.extern.slf4j.Slf4j;

import java.io.IOException;

import static java.lang.String.format;

@Slf4j
class SendGridMailPostRequest extends Request {

    private static final Method METHOD = Method.POST;

    SendGridMailPostRequest(Mail mail) {
        setMethod(METHOD);
        setEndpoint("mail/send");
        try {
            setBody(mail.build());
        } catch (IOException e) {
            log.error(format("Error creating %s request for mail %s", METHOD, mail), e);
        }
    }
}
