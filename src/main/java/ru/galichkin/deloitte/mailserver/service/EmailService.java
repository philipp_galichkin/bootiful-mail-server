package ru.galichkin.deloitte.mailserver.service;

import ru.galichkin.deloitte.common.domain.BasicMail;
import ru.galichkin.deloitte.mailserver.domain.SendEmailResponse;

public interface EmailService {
    SendEmailResponse send(BasicMail email);
}
