package ru.galichkin.deloitte.mailserver.exception;

public enum ErrorCode {
    SENDGRID_ERR, UNEXPECTED
}
