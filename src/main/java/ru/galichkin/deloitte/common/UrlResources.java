package ru.galichkin.deloitte.common;

import lombok.AccessLevel;
import lombok.NoArgsConstructor;

@NoArgsConstructor(access = AccessLevel.PRIVATE)
public final class UrlResources {
    public static final String EMAIL_RESOURCE_URL = "/mail";
    public static final String EMAIL_CALLBACK_RESOURCE_URL = EMAIL_RESOURCE_URL + "/callback";
}
