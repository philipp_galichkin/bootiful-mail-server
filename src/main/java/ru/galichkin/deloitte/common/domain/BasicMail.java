package ru.galichkin.deloitte.common.domain;

import java.util.List;
import java.util.UUID;

public interface BasicMail {

    UUID getId();

    String getFrom();

    List<String> getTos();

    List<String> getCcs();

    List<String> getBccs();

    String getSubject();

    String getBody();
}
